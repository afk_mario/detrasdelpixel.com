import { gql } from "graphql-request";

export const podcastQuery = gql`
  query {
    podcast(slug: "detras-del-pixel") {
      title
      slug
      image
      faq
      text
      smallText
      dateCreated
      iTunesURL
      googlePodcast
      spotify
      feedBurner
    }
  }
`;

export const singleEpisodeQuery = gql`
  query ($id: ID!) {
    episode(id: $id) {
      slug
      image
      title
      episodeNumber
      duration
      audioSize
      dateCreated
      audioOgg
      trackedMp3
      fullText
      smallText
    }
  }
`;

export const allEpisodesQuery = gql`
  query {
    allEpisodes(podcast_Slug: "detras-del-pixel") {
      edges {
        cursor
        node {
          id
          slug
          image
          title
          episodeNumber
          dateCreated
          smallText
          duration
          audioSize
          audioOgg
          audioMp3
          trackedMp3
        }
      }
    }
  }
`;

export const publishedEpisodesQuery = gql`
  query {
    allEpisodes(podcast_Slug: "detras-del-pixel", publish: true) {
      edges {
        cursor
        node {
          id
          slug
          image
          title
          episodeNumber
          dateCreated
          smallText
          duration
          audioSize
          audioOgg
          audioMp3
          trackedMp3
        }
      }
    }
  }
`;
