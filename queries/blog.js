import { gql } from "graphql-request";

export const allPostsQuery = gql`
  query {
    allPosts(tags_Name: "detras-del-pixel") {
      edges {
        cursor
        node {
          id
          title
          slug
          text
          image
          dateCreated
          author
          authorUrl
        }
      }
    }
  }
`;

export const publishedPostsQuery = gql`
  query {
    allPosts(tags_Name: "detras-del-pixel", publish: true) {
      edges {
        cursor
        node {
          id
          title
          slug
          text
          image
          dateCreated
          author
          authorUrl
        }
      }
    }
  }
`;

export const singlePostQuery = gql`
  query ($id: ID!, $cursor: String) {
    post(id: $id) {
      slug
      text
      title
      publish
      image
      dateCreated
      author
      authorUrl
    }
    prev: allPosts(
      first: 1
      publish: true
      tags_Name: "detras-del-pixel"
      after: $cursor
    ) {
      edges {
        cursor
        node {
          slug
          title
          image
        }
      }
    }
    next: allPosts(
      first: 1
      publish: true
      tags_Name: "detras-del-pixel"
      before: $cursor
    ) {
      edges {
        cursor
        node {
          slug
          title
          image
        }
      }
    }
  }
`;
