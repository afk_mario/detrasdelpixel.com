import Link from "next/link";
import { request } from "graphql-request";

import RichText from "components/rich-text";
import SEO from "components/seo";
import Layout from "components/layout";
import PostRow from "components/post-row";
import EpisodeRow from "components/episode-row";

import ArrowRight from "svg/arrow-right.svg";

import { podcastQuery, allEpisodesQuery } from "queries/podcast";
import { allPostsQuery } from "queries/blog";
import { API_URL } from "queries";

export default function Home({ podcast, allEpisodes, allPosts }) {
  const [episodeNode] = [...allEpisodes.edges].reverse();
  const [postNode] = allPosts.edges;

  const { node: episode } = episodeNode;
  const { node: post } = postNode;

  return (
    <Layout className="home" padding={false} podcast={podcast}>
      <SEO description={podcast.smallText} title={podcast.title} />
      <section className="hero-wrapper">
        <img className="logo" src={podcast.image} alt="Detrás del Pixel" />

        <div className="rich-text -big description -dark">
          <p>
            Un{" "}
            <Link href="/podcast">
              <a className="podcast">podcast</a>
            </Link>{" "}
            y{" "}
            <Link href="blog">
              <a className="blog">blog</a>
            </Link>{" "}
            acerca de todo lo que pase detrás de la industria de videojuegos.
          </p>
        </div>
      </section>
      <div className="wrapper">
        <div className="highlight-item-wrapper -episode">
          <Link href="/podcast">
            <a className="highlight-item-category">
              <div className="highlight-item-category-container">
                <ArrowRight className="arrow -right" />
                <h1>Podcast</h1>
              </div>
              <span className="-hide-mobile">Todos los episodios</span>
            </a>
          </Link>
          <EpisodeRow {...podcast} {...episode} active />
        </div>
        <div className="highlight-item-wrapper -post">
          <Link href="/blog">
            <a className="highlight-item-category">
              <div className="highlight-item-category-container">
                <ArrowRight className="arrow -right" />
                <h1>Blog</h1>
              </div>
              <span className="-hide-mobile">Todos los posts</span>
            </a>
          </Link>
          <PostRow className="-post" active {...post} />
        </div>
      </div>
    </Layout>
  );
}

export async function getStaticProps(context) {
  const { podcast } = await request(API_URL, podcastQuery);
  const { allEpisodes } = await request(API_URL, allEpisodesQuery);
  const { allPosts } = await request(API_URL, allPostsQuery);

  return {
    props: { podcast, allEpisodes, allPosts },
  };
}
