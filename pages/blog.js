import React from "react";
import PropTypes from "prop-types";
import { request } from "graphql-request";

import SEO from "components/seo";
import Layout from "components/layout";
import PostRow from "components/post-row";
import RichText from "components/rich-text";

import { podcastQuery, allEpisodesQuery } from "queries/podcast";
import { publishedPostsQuery } from "queries/blog";
import { API_URL } from "queries";

const description = `Espacio para las personas que ven a los videojuegos no solo como una forma de entretenimiento, sino como un proceso artístico especializado.`;

function Blog({ podcast, allPosts }) {
  const [featuredPost] = allPosts.edges;
  const { node: highlight } = featuredPost;

  return (
    <Layout className="blog" podcast={podcast}>
      <SEO description={description} title="Blog" />
      <div className="wrapper">
        <div className="blog-info">
          <h1 className="main-title">Blog</h1>
          <RichText className="blog-description" text={description} />
        </div>
        <PostRow key={highlight.slug} {...highlight} active />
        <section className="post-list">
          {allPosts.edges.map(({ node }, i) =>
            i !== 0 ? <PostRow key={node.slug} {...node} /> : null
          )}
        </section>
      </div>
    </Layout>
  );
}

Blog.propTypes = {
  allPosts: PropTypes.shape({
    edges: PropTypes.arrayOf(
      PropTypes.shape({
        node: PropTypes.shape({
          slug: PropTypes.string.isRequired,
        }).isRequired,
      }).isRequired
    ),
  }).isRequired,
};

export async function getStaticProps(context) {
  const { podcast } = await request(API_URL, podcastQuery);
  const { allPosts } = await request(API_URL, publishedPostsQuery);

  return {
    props: { allPosts, podcast },
  };
}

export default Blog;
