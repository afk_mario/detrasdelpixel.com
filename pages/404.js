import React from "react";
import PropTypes from "prop-types";
import { useRouter } from "next/router";

import SEO from "components/seo";
import Layout from "components/layout";

function E404() {
  const { pathname } = useRouter();

  return (
    <Layout className="e404">
      <SEO title="ERROR" />
      <div className="wrapper">
        <h1>Error 404</h1>
        <div className="rich-text">
          <p>No se dónde está la página:</p>
          <pre>{pathname}</pre>
          <p>
            Pero mándame un tweet{" "}
            <a href="https://twitter.com/afk_mario/">@afk_mario</a> e intentaré
            arreglarlo
          </p>
        </div>
      </div>
    </Layout>
  );
}

E404.propTypes = {};

export default E404;
