import "modern-css-reset";
import "rc-slider/assets/index.css";

import "styles/global.css";
import "styles/colors.css";
import "styles/gradients.css";
import "styles/mesurements.css";
import "styles/fonts.css";
import "styles/global.css";
import "styles/podcast-links.css";
import "styles/generic-info-page.css";

import "components/header/style.css";
import "components/footer/style.css";
import "components/layout/style.css";
import "components/social/style.css";
import "components/listen-on/style.css";
import "components/rich-text/style.css";
import "components/audio-player/style.css";

import "components/podcast-about/style.css";

import "components/episode-row/style.css";
import "components/min-list/style.css";

import "components/post-row/style.css";
import "components/post-attributes/style.css";
import "components/post-navigation/style.css";

import "pages/home.css";
import "pages/podcast.css";
import "pages/blog.css";
import "pages/404.css";

import "components/post-single/style.css";
import "components/episode-single/style.css";

import { AudioPlayerProvider } from "react-use-audio-player";

function MyApp({ Component, pageProps }) {
  return (
    <AudioPlayerProvider>
      <Component {...pageProps} />
    </AudioPlayerProvider>
  );
}

export default MyApp;
