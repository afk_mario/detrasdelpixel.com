import React from "react";
import PropTypes from "prop-types";
import { request, gql } from "graphql-request";

import SEO from "components/seo";
import Layout from "components/layout";
import ListenOn from "components/listen-on";
import RichText from "components/rich-text";

import { podcastQuery } from "queries/podcast";
import { API_URL } from "queries";

function AcercaDe({ podcast }) {
  return (
    <Layout className="faq-page generic-info-page" podcast={podcast}>
      <SEO description={podcast.smallText} title="Contacto" />
      <div className="wrapper">
        <div className="podcast-info">
          <img className="logo" src={podcast.image} alt={podcast.title} />
          <div className="podcast-links">
            <ListenOn {...podcast} />
          </div>
        </div>
        <RichText className="-big" text={podcast.text} />
      </div>
    </Layout>
  );
}

AcercaDe.propTypes = {
  podcast: PropTypes.shape({
    smallText: PropTypes.string,
    image: PropTypes.string,
    title: PropTypes.string,
    faq: PropTypes.string,
    text: PropTypes.string,
    iTunesURL: PropTypes.string.isRequired,
    spotify: PropTypes.string.isRequired,
    googlePodcast: PropTypes.string.isRequired,
    feedBurner: PropTypes.string.isRequired,
  }).isRequired,
};

export async function getStaticProps(context) {
  const { podcast } = await request(API_URL, podcastQuery);

  return {
    props: { podcast },
  };
}
export default AcercaDe;
