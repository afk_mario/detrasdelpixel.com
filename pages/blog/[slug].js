import React from "react";
import PropTypes from "prop-types";
import truncate from "truncate";
import { useRouter } from "next/router";
import { request, gql } from "graphql-request";

import { getDiscussUrl } from "lib";

import SEO from "components/seo";
import RichText from "components/rich-text";
import Layout from "components/layout";
import PostNavigation from "components/post-navigation";
import PostAttributes from "components/post-attributes";

import Twitter from "svg/twitter.svg";

import { API_URL } from "queries";
import { podcastQuery } from "queries/podcast";
import {
  allPostsQuery,
  publishedPostsQuery,
  singlePostQuery,
} from "queries/blog";

function PostSingle({ post, prev, next, podcast }) {
  const router = useRouter();
  const { text, title, image, dateCreated, author, authorUrl, publish } = post;

  const prevEdges = prev != null ? prev.edges : [];
  const nextEdges = next != null ? next.edges : [];

  const [prevArticle] = prevEdges;
  const [nextArticle] = nextEdges;

  const truncateText = truncate(post.text, 140);

  const discussUrl = getDiscussUrl({
    siteUrl: "",
    pathName: router.pathname,
  });

  return (
    <Layout className="post-single" padding={false} podcast={podcast}>
      <SEO description={truncateText} title={title} image={image} />

      <div className="post-cover">
        <div className="post-image-wrapper">
          {image ? <img src={image} alt={title} /> : null}
        </div>
        <div className="post-info">
          <div className="wrapper">
            <h1>{title}</h1>
            <PostAttributes
              text={text}
              date={dateCreated}
              author={author}
              authorUrl={authorUrl}
            />
          </div>
        </div>
      </div>
      <div className="gradient-wrapper">
        <div className="wrapper">
          <div className="gradient-ddp -small" />
        </div>
      </div>
      <div className="wrapper">
        <RichText text={text} limit />
        {publish ? (
          <>
            <div className="post-extras">
              <a
                className="post-discuss"
                href={discussUrl}
                target="_blank"
                rel="noopener noreferrer"
              >
                <Twitter className="twitter" />
                Comenta en Twitter
              </a>
            </div>
            <div className="gradient-ddp -small" />
            <div className="post-navigation">
              {nextArticle ? (
                <PostNavigation {...nextArticle.node} direction="next" />
              ) : null}
              {prevArticle ? (
                <PostNavigation {...prevArticle.node} direction="prev" />
              ) : null}
            </div>
          </>
        ) : null}
      </div>
    </Layout>
  );
}

PostSingle.propTypes = {
  post: PropTypes.shape({
    publish: PropTypes.bool.isRequired,
    slug: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    dateCreated: PropTypes.string.isRequired,
    author: PropTypes.string,
    authorUrl: PropTypes.string,
  }),
  next: PropTypes.shape({
    edges: PropTypes.arrayOf(
      PropTypes.shape({
        node: PropTypes.shape({
          slug: PropTypes.string,
          title: PropTypes.string,
          image: PropTypes.string.isRequired,
        }),
      })
    ),
  }),
  prev: PropTypes.shape({
    edges: PropTypes.arrayOf(
      PropTypes.shape({
        node: PropTypes.shape({
          slug: PropTypes.string,
          title: PropTypes.string,
          image: PropTypes.string.isRequired,
        }),
      })
    ),
  }),
};

export async function getStaticPaths() {
  const { allPosts } = await request(API_URL, allPostsQuery);
  const paths = allPosts.edges.map(({ node }) => {
    const { slug } = node;
    return {
      params: { slug },
    };
  });

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const { allPosts } = await request(API_URL, allPostsQuery);
  const { podcast } = await request(API_URL, podcastQuery);

  const found = allPosts.edges.find((item) => item.node.slug == params.slug);

  const variables = {
    id: found.node.id,
    cursor: found.cursor,
  };

  const { post, prev, next } = await request(
    API_URL,
    singlePostQuery,
    variables
  );

  return {
    props: { allPosts, post, prev, next, podcast },
  };
}

export default PostSingle;
