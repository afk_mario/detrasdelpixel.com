import React from "react";
import PropTypes from "prop-types";
import { request, gql } from "graphql-request";
import { useRouter } from "next/router";

import { formatBytes, getDesktopDate, getMobileDate } from "lib";

import SEO from "components/seo";
import Layout from "components/layout";
import RichText from "components/rich-text";
import AudioPlayer from "components/audio-player";

import { API_URL } from "queries";
import {
  allEpisodesQuery,
  podcastQuery,
  singleEpisodeQuery,
} from "queries/podcast";

function EpisodeSingle({ podcast, episode }) {
  const router = useRouter();
  const {
    title,
    episodeNumber,
    smallText,
    fullText,
    image,
    dateCreated,
    audioSize,
    trackedMp3,
    audioOgg,
  } = episode;

  const episodeTitle = `${episodeNumber.toString().padStart(2, 0)} - ${title}`;
  return (
    <Layout className="episode-single" padding={false} podcast={podcast}>
      <SEO description={smallText} title={title} image={image} />
      {image ? (
        <div className="episode-image-wrapper">
          <img src={image} alt={title} />
        </div>
      ) : null}
      <div className="gradient-ddp -small" />
      <div className="wrapper">
        <div className="episode-info">
          <h2 className="episode-title">{episodeTitle}</h2>
          <div className="episode-data">
            <span className="episode-date -hide-desktop">
              {getMobileDate(dateCreated)}
            </span>
            <span className="episode-date -hide-mobile">
              {getDesktopDate(dateCreated)}
            </span>
            <span className="episode-size">{formatBytes(audioSize)}</span>
          </div>
        </div>

        <AudioPlayer trackedMp3={trackedMp3} audioOgg={audioOgg} />
        <div className="episode-description">
          <RichText text={fullText} />
        </div>
      </div>
    </Layout>
  );
}

EpisodeSingle.propTypes = {
  episode: PropTypes.shape({
    title: PropTypes.string,
    episodeNumber: PropTypes.number,
    smallText: PropTypes.string,
    fullText: PropTypes.string,
    image: PropTypes.string,
    dateCreated: PropTypes.string,
    audioSize: PropTypes.number,
    trackedMp3: PropTypes.string,
    audioOgg: PropTypes.string,
  }).isRequired,
};

export async function getStaticPaths() {
  const { allEpisodes } = await request(API_URL, allEpisodesQuery);
  const paths = allEpisodes.edges.map(({ node }) => {
    const { slug } = node;
    return {
      params: { slug },
    };
  });

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const { allEpisodes } = await request(API_URL, allEpisodesQuery);
  const { podcast } = await request(API_URL, podcastQuery);

  const found = allEpisodes.edges.find((item) => item.node.slug == params.slug);

  const variables = {
    id: found.node.id,
    cursor: found.cursor,
  };

  const { episode } = await request(API_URL, singleEpisodeQuery, variables);

  return {
    props: { episode, podcast },
  };
}

export default EpisodeSingle;
