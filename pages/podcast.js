import React from "react";
import PropTypes from "prop-types";
import { request } from "graphql-request";

import { API_URL } from "queries";
import { podcastQuery, publishedEpisodesQuery } from "queries/podcast";

import SEO from "components/seo";
import Layout from "components/layout";
import EpisodeRow from "components/episode-row";
import MinList from "components/min-list";
import PodcastAbout from "components/podcast-about";

function Podcast({ podcast, allEpisodes }) {
  return (
    <Layout className="podcast" podcast={podcast}>
      <SEO description={podcast.smallText} title="Podcast" />
      <div className="wrapper">
        <MinList list={allEpisodes.edges} />
        <div>
          <PodcastAbout podcast={podcast} />
          <section className="episode-list">
            {[...allEpisodes.edges].reverse().map(({ node }, i) => (
              <EpisodeRow
                key={node.slug}
                {...podcast}
                {...node}
                active={i === 0}
                className={i === 0 ? "-expand" : undefined}
              />
            ))}
          </section>
        </div>
      </div>
    </Layout>
  );
}

export async function getStaticProps(context) {
  const { podcast } = await request(API_URL, podcastQuery);
  const { allEpisodes } = await request(API_URL, publishedEpisodesQuery);

  return {
    props: { podcast, allEpisodes },
  };
}

Podcast.propTypes = {
  podcast: PropTypes.shape({
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    smallText: PropTypes.string.isRequired,
  }).isRequired,
  allEpisodes: PropTypes.shape({
    edges: PropTypes.arrayOf(
      PropTypes.shape({
        node: PropTypes.shape({
          slug: PropTypes.string.isRequired,
        }).isRequired,
      }).isRequired
    ),
  }).isRequired,
};

export default Podcast;
