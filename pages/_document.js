import { Html, Head, Main, NextScript } from "next/document";
import siteMetadata from "site-metadata";

export default function Document() {
  return (
    <Html>
      <Head>
        <html lang={siteMetadata.siteLanguage} />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link
          href="https://fonts.googleapis.com/css2?family=Lora:wght@400;700&family=Montserrat:wght@700;800&family=Roboto:wght@400;700&display=swap"
          rel="stylesheet"
        />
        <link
          rel="alternate"
          type="application/rss+xml"
          title={`${siteMetadata.title} podcast`}
          href="http://feeds.ellugar.co/detras-del-pixel"
        />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
