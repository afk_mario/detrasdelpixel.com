import React from "react";
import PropTypes from "prop-types";
import { Rss, Volume2, Download } from "react-feather";
import Link from "next/link";

import Google from "svg/google.svg";
import Podcast from "svg/podcast.svg";
import Spotify from "svg/spotify.svg";

function EpisodeLinks({ podcast, slug, audioOgg }) {
  const { iTunesURL, googlePodcast, spotify, feedBurner } = podcast;

  return (
    <div className="podcast-links">
      <ul className="listen-on">
        <li>
          <Link href={`/episodio/${slug}`}>
            <a className="listen">
              <Volume2 />
              escucha
            </a>
          </Link>
        </li>
        <li>
          <a href={audioOgg} download={`${slug}.ogg`} className="download">
            <Download />
            descarga
          </a>
        </li>

        <li>
          <a
            rel="noopener noreferrer"
            target="_blank"
            className="itunes"
            href={iTunesURL}
          >
            <Podcast />
            iTunes
          </a>
        </li>
        <li>
          <a
            rel="noopener noreferrer"
            target="_blank"
            className="spotify"
            href={spotify}
          >
            <Spotify />
            Spotify
          </a>
        </li>
        <li>
          <a
            rel="noopener noreferrer"
            target="_blank"
            className="icon google"
            href={googlePodcast}
          >
            <Google className="google-podcast-icon" />
            <span>Google</span>
          </a>
        </li>
        <li>
          <a
            rel="noopener noreferrer"
            target="_blank"
            className="rss"
            href={feedBurner}
          >
            <Rss />
            rss
          </a>
        </li>
      </ul>
    </div>
  );
}

EpisodeLinks.propTypes = {
  slug: PropTypes.string.isRequired,
  audioOgg: PropTypes.string.isRequired,
};

EpisodeLinks.defaultProps = {};

export default EpisodeLinks;
