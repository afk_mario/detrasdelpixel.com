import React from 'react';
import PropTypes from 'prop-types';

function Iframe({ data: { hProperties } }) {
  const { allowfullscreen, frameborder, height, src, width } = hProperties;
  return (
    <iframe
      title={src}
      src={src}
      allowFullScreen={allowfullscreen}
      frameBorder={frameborder}
      height={height}
      width={width}
    />
  );
}

Iframe.propTypes = {
  data: PropTypes.shape({
    hProperties: PropTypes.shape({
      allowfullscreen: PropTypes.bool,
      frameborder: PropTypes.number,
      height: PropTypes.number,
      width: PropTypes.number,
      src: PropTypes.string,
    }),
  }).isRequired,
};

export default Iframe;
