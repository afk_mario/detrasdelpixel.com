import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import ReactMarkdown from "react-markdown";
// TODO: find a way to insert youtube videos again
// import remarkIframe from 'remark-iframes';
// import Iframe from './iframe';
// import 'regenerator-runtime/runtime';

const renderParagraph = ({ props }) => {
  const { children } = props;
  if (
    children &&
    children[0] &&
    children.length === 1 &&
    children[0].props &&
    children[0].props.src
  ) {
    return children;
  }

  return <p>{children}</p>;
};

const renderLink = ({ props }) => (
  <a {...props} href={props.href} target="_blank" rel="noopener noreferrer">
    {props.children}
  </a>
);

export const ytOptions = {
  "www.youtube.com": {
    tag: "iframe",
    width: 560,
    height: 315,
    disabled: false,
    replace: [
      ["watch?v=", "embed/"],
      ["http://", "https://"],
    ],
    thumbnail: {
      format: "http://img.youtube.com/vi/{id}/0.jpg",
      id: ".+/(.+)$",
    },
    removeAfter: "&",
  },
};

// const plugins = [[remarkIframe, ytOptions]];
const remarkPlugins = [];

function RichText({ text, limit, className, ...rest }) {
  const customClassName = classNames("rich-text", className, {
    "-limit": limit,
  });

  return (
    <ReactMarkdown
      className={customClassName}
      remarkPlugins={remarkPlugins}
      components={{
        link: renderLink,
        paragraph: renderParagraph,
      }}
      {...rest}
    >
      {text}
    </ReactMarkdown>
  );
}

RichText.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string,
  limit: PropTypes.bool,
};

RichText.defaultProps = {
  className: undefined,
  text: undefined,
  limit: false,
};

export default RichText;
