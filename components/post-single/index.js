import React from "react";
import PropTypes from "prop-types";
import truncate from "truncate";

import RichText from "components/rich-text";
import Layout from "components/layout";
import SEO from "components/seo";
import PostNavigation from "components/post-navigation";
import PostAttributes from "components/post-attributes";
import { getDiscussUrl } from "lib";

import Twitter from "svg/twitter.svg";

function PostSingle(props) {
  const { data, location } = props;
  const { post, prev, next } = data.ellugar;
  const { siteUrl } = data.site.siteMetadata;
  const { text, title, image, dateCreated, author, authorUrl, publish } = post;

  const prevEdges = prev != null ? prev.edges : [];
  const nextEdges = next != null ? next.edges : [];

  const [prevArticle] = prevEdges;
  const [nextArticle] = nextEdges;

  const truncateText = truncate(post.text, 140);

  const discussUrl = getDiscussUrl({
    siteUrl,
    pathName: location.pathname,
  });

  return (
    <Layout className="post-single" padding={false}>
      <SEO
        description={truncateText}
        title={title}
        image={image}
        pathName={location.pathname}
      />
      <div className="post-cover">
        <div className="post-image-wrapper">
          {image ? <img src={image} alt={title} /> : null}
        </div>
        <div className="post-info">
          <div className="wrapper">
            <h1>{title}</h1>
            <PostAttributes
              text={text}
              date={dateCreated}
              author={author}
              authorUrl={authorUrl}
            />
          </div>
        </div>
      </div>
      <div className="gradient-wrapper">
        <div className="wrapper">
          <div className="gradient-ddp -small" />
        </div>
      </div>
      <div className="wrapper">
        <RichText text={text} limit />
        {publish ? (
          <>
            <div className="post-extras">
              <a
                className="post-discuss"
                href={discussUrl}
                target="_blank"
                rel="noopener noreferrer"
              >
                <Twitter className="twitter" />
                Comenta en Twitter
              </a>
            </div>
            <div className="gradient-ddp -small" />
            <div className="post-navigation">
              {nextArticle ? (
                <PostNavigation {...nextArticle.node} direction="next" />
              ) : null}
              {prevArticle ? (
                <PostNavigation {...prevArticle.node} direction="prev" />
              ) : null}
            </div>
          </>
        ) : null}
      </div>
    </Layout>
  );
}

PostSingle.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  data: PropTypes.shape({
    site: PropTypes.shape({
      siteMetadata: PropTypes.shape({
        siteUrl: PropTypes.string.isRequired,
      }),
    }).isRequired,
    ellugar: PropTypes.shape({
      post: PropTypes.shape({
        publish: PropTypes.bool.isRequired,
        slug: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        image: PropTypes.string.isRequired,
        dateCreated: PropTypes.string.isRequired,
        author: PropTypes.string,
        authorUrl: PropTypes.string,
      }),
      next: PropTypes.shape({
        edges: PropTypes.arrayOf(
          PropTypes.shape({
            node: PropTypes.shape({
              slug: PropTypes.string,
              title: PropTypes.string,
              image: PropTypes.string.isRequired,
            }),
          })
        ),
      }),
      prev: PropTypes.shape({
        edges: PropTypes.arrayOf(
          PropTypes.shape({
            node: PropTypes.shape({
              slug: PropTypes.string,
              title: PropTypes.string,
              image: PropTypes.string.isRequired,
            }),
          })
        ),
      }),
    }),
  }).isRequired,
};

// export const query = graphql`
//   query ($id: ID!, $cursor: String) {
//     site {
//       siteMetadata {
//         siteUrl
//       }
//     }
//     ellugar {
//       post(id: $id) {
//         slug
//         text
//         title
//         publish
//         image
//         dateCreated
//         author
//         authorUrl
//       }
//       prev: allPosts(
//         first: 1
//         publish: true
//         tags_Name: "detras-del-pixel"
//         after: $cursor
//       ) {
//         edges {
//           cursor
//           node {
//             slug
//             title
//             image
//           }
//         }
//       }
//       next: allPosts(
//         first: 1
//         publish: true
//         tags_Name: "detras-del-pixel"
//         before: $cursor
//       ) {
//         edges {
//           cursor
//           node {
//             slug
//             title
//             image
//           }
//         }
//       }
//     }
//   }
// `;

export default PostSingle;
