import React from "react";
import PropTypes from "prop-types";
import Link from "next/link";

function PodcastAbout({ podcast }) {
  return (
    <section className="podcast-about">
      <img className="logo" src={podcast.image} alt="Detrás del Pixel" />
      <div className="rich-text -big description">
        <p>
          Un podcast{" "}
          <Link href="/acerca-de">
            <a>acerca de</a>
          </Link>{" "}
          lo que pasa detrás del desarrollo de videojuegos, conducido por{" "}
          <a
            rel="noopener noreferrer"
            target="_blank"
            href="https://twitter.com/afk_mario"
          >
            Mario Carballo Zama
          </a>
          .
        </p>
        <p>
          Puedes escucharlo en{" "}
          <a
            rel="noopener noreferrer"
            target="_blank"
            href={podcast.iTunesURL}
            className="itunes"
          >
            iTunes
          </a>
          ,{" "}
          <a
            rel="noopener noreferrer"
            target="_blank"
            href={podcast.googlePodcast}
            className=""
          >
            Google
          </a>
          ,{" "}
          <a
            rel="noopener noreferrer"
            target="_blank"
            href={podcast.spotify}
            className="spotify"
          >
            Spotify
          </a>
          , o suscribirte por{" "}
          <a
            rel="noopener noreferrer"
            target="_blank"
            href={podcast.feedBurner}
            className="rss"
          >
            rss
          </a>
          .
        </p>
      </div>
    </section>
  );
}

PodcastAbout.propTypes = {
  podcast: PropTypes.shape({
    spotify: PropTypes.string,
    googlePodcast: PropTypes.string,
    iTunesURL: PropTypes.string,
    feedBurner: PropTypes.string,
    image: PropTypes.string,
  }).isRequired,
};

export default PodcastAbout;
