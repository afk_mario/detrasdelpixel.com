import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";

import MiniLogo from "svg/mini-logo.svg";

import Social from "components/social";

const routes = [
  {
    path: "/podcast",
    label: "podcast",
  },
  {
    path: "/blog",
    label: "Blog",
  },
  {
    path: "/faq",
    label: "F.A.Q.",
  },
  {
    path: "/acerca-de",
    label: "Acerca de",
  },
  {
    path: "/contacto",
    label: "Contacto",
  },
];

function Header() {
  const { pathname } = useRouter();
  return (
    <header id="header">
      <div className="wrapper">
        <Link href="/">
          <a aria-label="Home Link">
            <MiniLogo className="mini-logo" />
          </a>
        </Link>
        <nav>
          <ul>
            {routes.map((route) => {
              const isActive = pathname.startsWith(route.path);
              return (
                <li key={route.path}>
                  <Link href={route.path}>
                    <a className={`route`} data-active={isActive}>
                      <span>{route.label}</span>
                    </a>
                  </Link>
                </li>
              );
            })}
          </ul>
        </nav>
        <Social className="-hide-mobile" />
      </div>
    </header>
  );
}

export default Header;
