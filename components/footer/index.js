import React from "react";
import PropTypes from "prop-types";

import ListenOn from "components/listen-on";
import Social from "components/social";

function Footer({ podcast }) {
  return (
    <footer id="footer">
      <div className="wrapper">
        <article>
          <h2>Escucha el podcast en</h2>
          <ListenOn className="-dark" {...podcast} />
        </article>
        <article>
          <h2>Síguenos en</h2>
          <Social className="-dark" />
        </article>
      </div>
    </footer>
  );
}

Footer.propTypes = {
  podcast: PropTypes.shape({
    iTunesURL: PropTypes.string.isRequired,
    spotify: PropTypes.string.isRequired,
    googlePodcast: PropTypes.string.isRequired,
    feedBurner: PropTypes.string.isRequired,
  }).isRequired,
};

export default Footer;
