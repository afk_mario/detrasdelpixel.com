import PropTypes from "prop-types";
import React from "react";
import Head from "next/head";

function Fb({ url, name, type, title, description, image, locale, appId }) {
  return <Head>
    {name && <meta property="og:site_name" content={name} />}
    <meta property="og:locale" content={locale} />
    <meta property="og:url" content={url} />
    <meta property="og:type" content={type} />
    <meta property="og:title" content={title} />
    <meta property="og:description" content={description} />
    <meta property="og:image" content={image} />
    <meta property="og:image:alt" content={description} />
    <meta property="fb:app_id" content={appId} />
  </Head>
}

Fb.propTypes = {
  url: PropTypes.string.isRequired,
  locale: PropTypes.string.isRequired,
  type: PropTypes.string,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  appId: PropTypes.string.isRequired,
  name: PropTypes.string,
};

Fb.defaultProps = {
  type: "website",
  name: null,
};

export default Fb;
