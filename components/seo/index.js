import React from "react";
import PropTypes from "prop-types";
import Head from "next/head";
import { useRouter } from "next/router";

import siteMetadata from "site-metadata";

import Facebook from "./fb";
import Twitter from "./tw";
import getSchemaOrg from "./shchema-org";

function SEO({ description, keywords, title, image }) {
  const { pathname } = useRouter();
  const { appId } = siteMetadata;
  const url = `${siteMetadata.siteUrl}${pathname || ""}`;

  const seo = {
    title,
    description,
    image,
    url,
  };

  const schemaOrgWebPage = getSchemaOrg({
    title,
    seo,
    ...siteMetadata,
  });

  return (
    <>
      <Head>
        <title>{`${title} | DDP`}</title>
        <meta name="description" content={seo.description} />
        <meta name="apple-itunes-app" content="app-id=1250746147" />
        <script type="application/ld+json">
          {JSON.stringify(schemaOrgWebPage)}
        </script>
      </Head>
      <Facebook
        description={seo.description}
        image={seo.image}
        title={seo.title}
        url={seo.url}
        locale={siteMetadata.ogLanguage}
        appId={appId}
      />
      <Twitter
        title={seo.title}
        image={seo.image}
        desc={seo.description}
        username="detrasdelpixel_"
      />
    </>
  );
}

SEO.propTypes = {
  description: PropTypes.string,
  keywords: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string.isRequired,
  image: PropTypes.string,
};

SEO.defaultProps = {
  image: siteMetadata.image,
  description: siteMetadata.description,
  keywords: [],
};

export default SEO;
