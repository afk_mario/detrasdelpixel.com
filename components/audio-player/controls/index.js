import React from "react";
import PropTypes from "prop-types";
import Slider from "rc-slider";
import { Play, Pause, Volume, Volume1, Volume2 } from "react-feather";

import Duration from "components/duration";

function Controls({
  percentComplete,
  loaded,
  duration,
  isPlaying,
  volume,
  onVolumeChange,
  onPlay,
  onPause,
  onStartSeek,
  onFinishSeek,
  onSeek,
}) {
  const played = percentComplete / 100;
  return (
    <>
      <div className="player-controls">
        {isPlaying && (
          <button
            type="button"
            className="button"
            onClick={onPause}
            aria-label="play"
          >
            <Pause />
          </button>
        )}
        {!isPlaying && (
          <button
            type="button"
            className="button"
            onClick={onPlay}
            aria-label="pause"
          >
            <Play />
          </button>
        )}
      </div>

      <div className="audio-bar">
        <div className="audio-bar-lines">
          <progress max={1} value={1} className="progressbar total" />
          <progress max={1} value={loaded} className="progressbar loading" />
          <progress max={1} value={played} className="progressbar position" />
          <Slider
            className="progressbar"
            min={0}
            max={1}
            value={played}
            step={0.01}
            onMouseDown={onStartSeek}
            onChange={onSeek}
            onAfterChange={onFinishSeek}
          />
        </div>
      </div>

      <div className="duration-info">
        <Duration className="duration" seconds={duration * played} />
        <span className="divider">/</span>
        <Duration className="duration" seconds={duration} />
      </div>

      <div className="volume-controls">
        {volume === 0 && <Volume />}
        {volume > 0 && volume <= 0.5 && <Volume1 />}
        {volume > 0.5 && <Volume2 />}
        <div className="volume-bar-lines">
          <Slider
            value={volume}
            min={0}
            max={1.0}
            step={0.1}
            onChange={onVolumeChange}
          />
        </div>
      </div>
    </>
  );
}

Controls.propTypes = {
  percentComplete: PropTypes.number.isRequired,
  duration: PropTypes.number.isRequired,
  isPlaying: PropTypes.bool.isRequired,
  volume: PropTypes.number,
  onVolumeChange: PropTypes.func.isRequired,
  onPlay: PropTypes.func.isRequired,
  onPause: PropTypes.func.isRequired,
  onStartSeek: PropTypes.func,
  onFinishSeek: PropTypes.func,
  onSeek: PropTypes.func.isRequired,
  loaded: PropTypes.number,
};

Controls.defaultProps = {
  volume: 1,
  loaded: 0,
  onStartSeek: () => {},
  onFinishSeek: () => {},
};

export default Controls;
