import React from "react";
import PropTypes from "prop-types";
import { useRouter } from "next/router";
import {
  useAudioPlayer,
  useAudioPosition,
  AudioPlayerProvider,
} from "react-use-audio-player";

import Controls from "./controls";

function AudioPlayer({ trackedMp3 }) {
  const router = useRouter();
  const audioPlayer = useAudioPlayer({
    src: trackedMp3,
    format: ["mp3"],
    autoplay: false,
    html5: false,
  });

  const {
    loading,
    stop,
    pause,
    ready,
    togglePlayPause,
    playing,
    volume,
    player,
  } = audioPlayer;

  const onRouteChange = React.useCallback(() => {
    pause();
  }, [pause]);

  React.useEffect(() => {
    router.events.on("routeChangeComplete", onRouteChange);
    return () => router.events.off("routeChangeComplete", onRouteChange);
  }, [router.events, onRouteChange]);

  const { percentComplete, duration, seek } = useAudioPosition({
    highRefreshRate: true,
  });

  const handleSeek = React.useCallback(
    (value) => seek(duration * value),
    [duration, seek]
  );

  const handleVolumeChange = React.useCallback(
    (value) => volume(value),
    [volume]
  );

  if (!ready && !loading) return "Algo malo paso ...";
  if (loading) return null;

  return (
    <div className="audio-player">
      <Controls
        percentComplete={percentComplete}
        duration={duration}
        volume={volume()}
        isPlaying={playing}
        onPlay={togglePlayPause}
        onPause={togglePlayPause}
        onSeek={handleSeek}
        onVolumeChange={handleVolumeChange}
      />
    </div>
  );
}

AudioPlayer.propTypes = {
  audioOgg: PropTypes.string,
  audioMp3: PropTypes.string,
  trackedMp3: PropTypes.string.isRequired,
};

export default AudioPlayer;
