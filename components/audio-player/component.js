import React, { useState, useRef } from "react";
import PropTypes from "prop-types";
import ReactPlayer from "react-player/file";

import Controls from "./controls";

function Component({ trackedMp3, audioOgg }) {
  const [volume, setVolume] = useState(1);
  const [duration, setDuration] = useState(0);
  const [played, setPlayed] = useState(0);
  const [loaded, setLoaded] = useState(0);
  const [isPlaying, setPlaying] = useState(false);
  const [seeking, setSeeking] = useState(false);
  const player = useRef(null);

  const handleStartSeek = () => {
    setSeeking(true);
  };
  const handleSeek = (value) => {
    setPlayed(parseFloat(value));
  };
  const handleFinishSeek = (value) => {
    setSeeking(false);
    player.current.seekTo(parseFloat(value));
  };
  const handleVolumeChange = (value) => {
    setVolume(parseFloat(value));
  };

  const handlePlay = () => {
    setPlaying(true);
  };

  const handlePause = () => {
    setPlaying(false);
  };

  return (
    <div className="audio-player">
      <ReactPlayer
        ref={player}
        url={[trackedMp3, audioOgg]}
        width="0%"
        height="auto"
        volume={volume}
        onReady={() => {}}
        onStart={() => {}}
        onError={(e) => console.error("Error", e)}
        playing={isPlaying}
        onDuration={(e) => {
          setDuration(e);
        }}
        onProgress={(e) => {
          if (seeking) return;
          setPlayed(e.played);
          setLoaded(e.loaded);
        }}
      />
      <Controls
        loaded={loaded}
        played={played}
        duration={duration}
        volume={volume}
        isPlaying={isPlaying}
        onPlay={handlePlay}
        onPause={handlePause}
        onFinishSeek={handleFinishSeek}
        onStartSeek={handleStartSeek}
        onSeek={handleSeek}
        onVolumeChange={handleVolumeChange}
      />
    </div>
  );
}

Component.propTypes = {
  audioOgg: PropTypes.string.isRequired,
  trackedMp3: PropTypes.string.isRequired,
};

export default Component;
