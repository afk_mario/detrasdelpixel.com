import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import truncate from "truncate";
import PostAttributes from "components/post-attributes";
import RichText from "components/rich-text";

function PostRow({
  title,
  image,
  slug,
  text,
  author,
  authorUrl,
  dateCreated,
  className,
  active,
}) {
  const customClassName = classNames("post-row", className, {
    "-active": active,
  });
  const truncateText = truncate(text, active ? 500 : 140);
  return (
    <div className={customClassName}>
      {image && (
        <>
          <a href={`/blog/${slug}`} className="img-wrapper">
            <img src={image} alt={title} />
          </a>
          <div className="gradient-ddp" />
        </>
      )}
      <div className="post-row-info">
        <div className="height-wrapper">
          <a href={`/blog/${slug}`}>
            <h2>{title}</h2>
          </a>
          <PostAttributes
            vertical={!active}
            text={text}
            date={dateCreated}
            author={author}
            authorUrl={authorUrl}
          />
        </div>
      </div>
      {active || (!image && text) ? (
        <div className="post-row-content">
          <RichText text={truncateText} disallowedElements={["img"]} />
        </div>
      ) : undefined}
    </div>
  );
}

PostRow.propTypes = {
  title: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  text: PropTypes.string,
  dateCreated: PropTypes.string.isRequired,
  author: PropTypes.string,
  authorUrl: PropTypes.string,
  image: PropTypes.string,
  className: PropTypes.string,
  active: PropTypes.bool,
};

PostRow.defaultProps = {
  image: undefined,
  className: undefined,
  active: false,
  text: undefined,
  author: undefined,
  authorUrl: undefined,
};

export default PostRow;
