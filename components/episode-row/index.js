import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import Link from "next/link";
import { Eye } from "react-feather";

import { getDesktopDate, getMobileDate } from "lib";

import RichText from "components/rich-text";
import EpisodeLinks from "components/episode-links";
import AudioPlayer from "components/audio-player";

function EpisodeRow({
  slug,
  image,
  title,
  episodeNumber,
  dateCreated,
  audioOgg,
  trackedMp3,
  audioMp3,
  fullText,
  smallText,
  iTunesURL,
  googlePodcast,
  spotify,
  feedBurner,
  className,
  active,
}) {
  const customClassName = classNames("episode-row", className, {
    "-active": active,
  });
  const episodeTitle = active
    ? smallText
    : `${episodeNumber.toString().padStart(2, 0)} - ${title}`;

  return (
    <div className={customClassName}>
      <div className="episode-row-flag">
        <Link href={`/episodio/${slug}`}>
          <a className="episode-cover">
            <img src={`${image}`} alt={slug} />
          </a>
        </Link>
        <div className="gradient-ddp -hide-desktop" />
        <div className="episode-info">
          <Link href={`/episodio/${slug}`}>
            <a className="episode-header">
              <div className="episode-attributes">
                <h2 className="episode-title">{episodeTitle}</h2>
                <div className="episode-meta">
                  <span className="episode-date -hide-desktop">
                    {getMobileDate(dateCreated)}
                  </span>
                  <span className="episode-date -hide-mobile">
                    {getDesktopDate(dateCreated)}
                  </span>
                  <span className="episode-notes">
                    <Eye style={{ width: "15px", height: "15px" }} /> notas
                  </span>
                </div>
              </div>
            </a>
          </Link>
          {!active ? <p className="episode-summary">{smallText}</p> : null}
          <EpisodeLinks
            slug={slug}
            audioOgg={audioOgg}
            podcast={{
              iTunesURL,
              googlePodcast,
              spotify,
              feedBurner,
            }}
          />
        </div>
      </div>
      <div className="gradient-ddp -hide-mobile" />
      {active ? (
        <AudioPlayer
          trackedMp3={trackedMp3}
          audioOgg={audioOgg}
          audioMp3={audioMp3}
        />
      ) : null}
      {fullText && active ? (
        <div className="episode-description">
          <RichText text={fullText} />
        </div>
      ) : null}
    </div>
  );
}

EpisodeRow.propTypes = {
  slug: PropTypes.string.isRequired,
  episodeNumber: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  dateCreated: PropTypes.string.isRequired,
  audioOgg: PropTypes.string.isRequired,
  trackedMp3: PropTypes.string.isRequired,
  fullText: PropTypes.string,
  smallText: PropTypes.string,
  className: PropTypes.string,
  active: PropTypes.bool,
};

EpisodeRow.defaultProps = {
  className: undefined,
  smallText: undefined,
  fullText: undefined,
  active: false,
};

export default EpisodeRow;
