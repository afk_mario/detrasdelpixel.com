import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { getDesktopDate, getMobileDate } from "lib";
import { Clock, Coffee } from "react-feather";
import readingTime from "reading-time";

function PostAttributes({
  text,
  date,
  author,
  authorUrl,
  vertical,
  className,
}) {
  const stats = text ? readingTime(text) : undefined;
  const customClassName = classNames("post-attributes", className, {
    "-vertical": vertical,
  });
  return (
    <div className={customClassName}>
      <div className="post-time-attributes">
        {stats ? (
          <div className="post-time-wrapper">
            <Coffee />
            {Math.floor(stats.minutes)} minutos
          </div>
        ) : null}
        <span className="post-date -hide-mobile">
          <Clock />
          {getDesktopDate(date)}
        </span>
        <span className="post-date -hide-desktop">
          <Clock />
          {getMobileDate(date)}
        </span>
      </div>
      {authorUrl && author ? (
        <span className="post-author-wrapper">
          Por{" "}
          <a className="post-author" href={authorUrl}>
            {author}
          </a>
        </span>
      ) : null}
    </div>
  );
}

PostAttributes.propTypes = {
  text: PropTypes.string,
  date: PropTypes.string.isRequired,
  author: PropTypes.string,
  authorUrl: PropTypes.string,
  vertical: PropTypes.bool,
  className: PropTypes.string,
};

PostAttributes.defaultProps = {
  text: undefined,
  author: undefined,
  authorUrl: undefined,
  vertical: false,
  className: undefined,
};

export default PostAttributes;
