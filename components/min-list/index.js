import React from "react";
import Link from "next/link";
import PropTypes from "prop-types";
import { ArrowUp } from "react-feather";

function MinList({ list }) {
  return (
    <aside className="min-list -hide-mobile">
      <ul>
        {list.map(({ node: { title, episodeNumber, slug } }) => (
          <li key={slug}>
            <Link href={`/episodio/${slug}`}>
              <a>
                <span className="item-number">
                  {episodeNumber.toString().padStart(2, 0)}
                </span>
                <span className="item-name"> - {title}</span>
              </a>
            </Link>
          </li>
        ))}
      </ul>

      <Link href="/podcast#">
        <a className="go-to-top">
          <span className="item-number">
            <ArrowUp />
          </span>
          <span className="item-name"> Ir arriba</span>
        </a>
      </Link>
    </aside>
  );
}

MinList.propTypes = {
  list: PropTypes.arrayOf(
    PropTypes.shape({
      episodeNumber: PropTypes.number,
      title: PropTypes.string,
      slug: PropTypes.string,
    })
  ).isRequired,
};

export default MinList;
