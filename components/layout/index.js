import React from "react";
import PropTypes from "prop-types";
import Header from "components/header";
import Footer from "components/footer";
import classNames from "classnames";

function Layout({ className, children, padding, podcast }) {
  const customClassName = classNames("page", className, {
    "-padding": padding,
  });
  return (
    <>
      <Header />
      <main className={customClassName}>{children}</main>
      <Footer podcast={podcast} />
    </>
  );
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  padding: PropTypes.bool,
  podcast: PropTypes.shape({
    iTunesURL: PropTypes.string.isRequired,
    spotify: PropTypes.string.isRequired,
    googlePodcast: PropTypes.string.isRequired,
    feedBurner: PropTypes.string.isRequired,
  }).isRequired,
};

Layout.defaultProps = {
  className: undefined,
  padding: true,
};

export default Layout;
