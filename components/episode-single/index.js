import React from "react";
import PropTypes from "prop-types";

import { formatBytes, getDesktopDate, getMobileDate } from "lib";

import Layout from "components/layout";
import SEO from "components/seo";
import RichText from "components/rich-text";

import AudioPlayer from "components/audio-player";

function EpisodeSingle({ data, location }) {
  const { episode } = data.ellugar;
  const {
    title,
    episodeNumber,
    smallText,
    fullText,
    image,
    dateCreated,
    audioSize,
    trackedMp3,
    audioOgg,
  } = episode;

  const episodeTitle = `${episodeNumber.toString().padStart(2, 0)} - ${title}`;
  return (
    <Layout className="episode-single" padding={false}>
      <SEO
        description={smallText}
        title={title}
        image={image}
        pathName={location.pathname}
      />
      {image ? (
        <div className="episode-image-wrapper">
          <img src={image} alt={title} />
        </div>
      ) : null}
      <div className="gradient-ddp -small" />
      <div className="wrapper">
        <div className="episode-info">
          <h2 className="episode-title">{episodeTitle}</h2>
          <div className="episode-data">
            <span className="episode-date -hide-desktop">
              {getMobileDate(dateCreated)}
            </span>
            <span className="episode-date -hide-mobile">
              {getDesktopDate(dateCreated)}
            </span>
            <span className="episode-size">{formatBytes(audioSize)}</span>
          </div>
        </div>

        <AudioPlayer trackedMp3={trackedMp3} audioOgg={audioOgg} />
        <div className="episode-description">
          <RichText text={fullText} />
        </div>
      </div>
    </Layout>
  );
}

EpisodeSingle.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  data: PropTypes.shape({
    ellugar: PropTypes.shape({
      episode: PropTypes.shape({
        title: PropTypes.string,
        episodeNumber: PropTypes.number,
        smallText: PropTypes.string,
        fullText: PropTypes.string,
        image: PropTypes.string,
        dateCreated: PropTypes.string,
        audioSize: PropTypes.number,
        trackedMp3: PropTypes.string,
        audioOgg: PropTypes.string,
      }),
    }).isRequired,
  }).isRequired,
};

// export const query = graphql`
//   query ($id: ID!) {
//     ellugar {
//       episode(id: $id) {
//         slug
//         image
//         title
//         episodeNumber
//         duration
//         audioSize
//         dateCreated
//         audioOgg
//         trackedMp3
//         fullText
//         smallText
//       }
//     }
//   }
// `;
//
export default EpisodeSingle;
