import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { Rss } from "react-feather";
import Google from "svg/google.svg";
import Podcast from "svg/podcast.svg";
import Spotify from "svg/spotify.svg";

function ListenOn({
  vertical,
  className,
  iTunesURL,
  spotify,
  googlePodcast,
  feedBurner,
}) {
  const customClassName = classNames("listen-on", className, {
    "-vertical": vertical,
  });

  const podcast = {};

  return (
    <ul className={customClassName}>
      <li>
        <a
          rel="noopener noreferrer"
          target="_blank"
          className="itunes"
          href={iTunesURL}
        >
          <Podcast />
          iTunes
        </a>
      </li>
      <li>
        <a
          rel="noopener noreferrer"
          target="_blank"
          className="spotify"
          href={spotify}
        >
          <Spotify />
          Spotify
        </a>
      </li>
      <li>
        <a
          rel="noopener noreferrer"
          target="_blank"
          className="icon google"
          href={googlePodcast}
        >
          <Google className="google-podcast-icon" />
          <span>Google</span>
        </a>
      </li>
      <li>
        <a
          rel="noopener noreferrer"
          target="_blank"
          className="rss"
          href={feedBurner}
        >
          <Rss />
          rss
        </a>
      </li>
    </ul>
  );
}

ListenOn.propTypes = {
  vertical: PropTypes.bool,
  className: PropTypes.string,
  iTunesURL: PropTypes.string.isRequired,
  spotify: PropTypes.string.isRequired,
  googlePodcast: PropTypes.string.isRequired,
  feedBurner: PropTypes.string.isRequired,
};

ListenOn.defaultProps = {
  vertical: false,
  className: undefined,
};

export default ListenOn;
