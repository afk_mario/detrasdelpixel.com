import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import Link from "next/link";

import ArrowLeft from "svg/arrow-left.svg";
import ArrowRight from "svg/arrow-right.svg";

function PostNavigation({ slug, title, image, direction, className }) {
  const customClassName = classNames("post-navigation-item", className);
  return (
    <Link href={`/blog/${slug}`} scroll={true}>
      <a className={customClassName}>
        {direction === "prev" ? (
          <ArrowRight className="arrow -right -hide-mobile" />
        ) : null}

        {direction === "next" ? (
          <ArrowLeft className="arrow -left -hide-mobile" />
        ) : null}
        {image ? (
          <div className="post-navigation-img">
            <img src={image} alt={title} />
          </div>
        ) : null}
        <div className="post-navigation-info">
          {direction === "prev" ? (
            <ArrowRight className="arrow -right -hide-desktop" />
          ) : null}

          {direction === "next" ? (
            <ArrowLeft className="arrow -left -hide-desktop" />
          ) : null}
          <h2 className="post-navigation-title">{title}</h2>
        </div>
      </a>
    </Link>
  );
}

PostNavigation.propTypes = {
  title: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  direction: PropTypes.oneOf(["next", "prev"]).isRequired,
  image: PropTypes.string,
  className: PropTypes.string,
};

PostNavigation.defaultProps = {
  image: undefined,
  className: undefined,
};

export default PostNavigation;
