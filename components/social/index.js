import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import Ellugar from "svg/ellugar.svg";

function Social({ showText, ellugar, className }) {
  const customClassName = classNames("social", className, {
    "-show-text": showText,
  });

  return (
    <ul className={customClassName}>
      <li>
        <a
          rel="noopener noreferrer"
          target="_blank"
          href="https://twitter.com/detrasdelpixel_"
          className="twitter"
        >
          tw.
        </a>
      </li>
      <li>
        <a
          rel="noopener noreferrer"
          target="_blank"
          href="https://facebook.com/detras-del-pixel"
          className="facebook"
        >
          fb.
        </a>
      </li>
      <li>
        <a
          rel="noopener noreferrer"
          target="_blank"
          href="https://instagram.com/detrasdelpixel"
          className="instagram"
        >
          ig.
        </a>
      </li>

      {ellugar ? (
        <li className="ellugar">
          <a
            rel="noopener noreferrer"
            target="_blank"
            href="https://ellugar.co"
            className="ellugar"
            aria-label="ellugar.co"
          >
            <Ellugar />
            {showText ? "ellugar" : null}
          </a>
        </li>
      ) : null}
    </ul>
  );
}

Social.propTypes = {
  showText: PropTypes.bool,
  ellugar: PropTypes.bool,
  className: PropTypes.string,
};

Social.defaultProps = {
  showText: false,
  ellugar: true,
  className: undefined,
};

export default Social;
