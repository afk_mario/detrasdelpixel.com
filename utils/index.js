exports.makePostPath = ({ slug }) => `/blog/${slug}`;

exports.makeEpisodePath = ({ slug }) => `/episodio/${slug}`;
