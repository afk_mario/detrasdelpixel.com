const siteMetadata = {
  appId: `2404228682980287`,
  title: `Detrás del Pixel`,
  author: `Mario Carballo Zama`,
  image: "/images/preview.jpg",
  siteUrl: `https://detrasdelpixel.com`,
  description: `Detrás del Pixel es un podcast en el cual se habla de historias y experiencias detrás del desarrollo de videojuegos.`,
  siteLanguage: "es",
  ogLanguage: "es-mx",
};

export default siteMetadata;
