export const dateConfigMobile = {
  year: '2-digit',
  month: '2-digit',
  day: '2-digit',
};

export const dateConfigDesktop = {
  year: '2-digit',
  month: 'short',
  day: '2-digit',
};

export const getMobileDate = date => new Date(date).toLocaleDateString('es-mx', dateConfigMobile);

export const getDesktopDate = date => new Date(date).toLocaleDateString('es-mx', dateConfigDesktop);

export const getDiscussUrl = ({siteUrl, pathName}) => {
  const twURL = 'https://mobile.twitter.com/search?q=';
  const postURL = `${siteUrl}${pathName}`;
  const postURI = encodeURIComponent(postURL);

  return `${twURL}${postURI}`;
}

export const formatBytes = (bytes, decimals) => {
  if (bytes === 0) return '0 Bytes';
  const k = 1024;
  const dm = decimals || 2;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  const i = Math.floor(Math.log(bytes) / Math.log(k));
  const num = parseFloat((bytes / k ** i).toFixed(dm));
  return `${num} ${sizes[i]}`;
};
