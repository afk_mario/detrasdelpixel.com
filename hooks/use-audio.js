import React, { useRef, useState } from "react";

const parseTimeRange = (ranges) =>
  ranges.length < 1
    ? {
        start: 0,
        end: 0,
      }
    : {
        start: ranges.start(0),
        end: ranges.end(0),
      };

function useAudio({ src, autoPlay = false, startPlaybackRate = 1 }) {
  const lockPlay = useRef(false);
  const ref = useRef(null);

  const [state, setOrgState] = useState({
    buffered: {
      start: 0,
      end: 0,
    },
    time: 0,
    duration: 0,
    paused: true,
    waiting: false,
    playbackRate: 1,
    endedCallback: null,
  });

  const setState = React.useCallback(
    (partState) => setOrgState({ ...state, ...partState }),
    [setOrgState, state]
  );

  const element = React.createElement("audio", {
    src,
    controls: false,
    ref,
    onPlay: () => setState({ paused: false }),
    onPause: () => setState({ paused: true }),
    onWaiting: () => setState({ waiting: true }),
    onPlaying: () => setState({ waiting: false }),
    onEnded: state.endedCallback,
    onDurationChange: () => {
      const el = ref.current;
      if (!el) {
        return;
      }
      const { duration, buffered } = el;
      setState({
        duration,
        buffered: parseTimeRange(buffered),
      });
    },
    onTimeUpdate: () => {
      const el = ref.current;
      if (!el) {
        return;
      }
      setState({ time: el.currentTime });
    },
    onProgress: () => {
      const el = ref.current;
      if (!el) {
        return;
      }
      setState({ buffered: parseTimeRange(el.buffered) });
    },
  });

  const controls = React.useMemo(
    () => ({
      play: () => {
        const el = ref.current;
        if (!el) {
          return undefined;
        }

        if (!lockPlay) {
          const promise = el.play();
          const isPromise = typeof promise === "object";

          if (isPromise) {
            lockPlay.current = true;
            const resetLock = () => {
              lockPlay.current = false;
            };
            promise.then(resetLock, resetLock);
          }

          return promise;
        }
        return undefined;
      },
      pause: () => {
        const el = ref.current;
        if (el && !lockPlay.current) {
          return el.pause();
        }
        return false;
      },
      seek: (time) => {
        const el = ref.current;
        if (!el || state.duration === undefined) {
          return null;
        }
        const nTime = Math.min(state.duration, Math.max(0, time));
        el.currentTime = nTime || 0;
        return nTime;
      },
      setPlaybackRate: (rate) => {
        const el = ref.current;
        if (!el || state.duration === undefined) {
          return;
        }

        setState({
          playbackRate: rate,
        });
        el.playbackRate = rate;
      },
      setEndedCallback: (callback) => {
        setState({ endedCallback: callback });
      },
    }),
    [setState, state.duration]
  );

  // useEffect(() => {
  //   const el = ref.current || {};
  //   setState({
  //     paused: el.paused,
  //   });

  //   controls.setPlaybackRate(startPlaybackRate);

  //   if (autoPlay && el.paused) {
  //     controls.play();
  //   }
  // }, [src, controls, startPlaybackRate, setState, autoPlay]);

  return { element, state, controls };
}

export default useAudio;
